$(document).ready(function() {
    var actions = {
        makeAjax : function(config){
            $.ajax({
                url: config.formUrl,
                type: config.formMethod,
                data: config.formData,
                beforeSend: function() {
                    $(config.submit_btn).button('loading');
                },
                success: function(data) {
                    var responseData = jQuery.parseJSON(data), klass;
                    config.submit_btn.button('reset');
                    //response conditional
                    switch(responseData.status){
                        case 'error':
                            klass = config.alert_class;
                            config.submit_btn.button('reset');
                            break;
                        case 'success':
                            klass = config.success_class;
                            break;
                    };
                    //show reponse message
                    config.responseMsg.fadeOut(200,function(){
                        $(this).removeClass('hide')
                            .addClass(klass).html(responseData.message)
                            .fadeIn(200,function(){
                                //set timeout to hide response message
                                setTimeout(function(){
                                    config.responseMsg.fadeOut(300,function(){
                                        $(this).removeClass(klass);
                                    });
                                },4000)
                            })
                    });

                },
                error: function (){},
                done: function(){
                    config.submit_btn.button('reset');
                }
            })
        }
    };

    //user register form
    $('form#mailing-list').bind('submit', function() {
        var form = $(this);
        actions.makeAjax({
            form: $(this),
            formData : form.serialize(), //serialize() gets all the form fields and all their values and puts them in key-value pairs inside a string.
            formUrl : form.attr('action'), //URL we want to send our data to
            formMethod : form.attr('method'),
            responseMsg : $('.join-response'),
            alert_class :'alert-error',
            success_class : 'alert-success',
            submit_btn: form.find('button[type="submit"]')
        });
        return false
    });

});
