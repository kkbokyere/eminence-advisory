<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Eminence Advisory - Coming Soon</title>
        <meta name="description" content="Eminence Advisory sets out to help businesses and entrepreneurs develop successful businesses in Africa.">
        <meta name="keywords" content="eminence, advisory, consultancy, entrepreneurs, africa">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="google-site-verification" content="Sez0lZY4KWs8pwLHGjYX7zKr4rhgYHP7VQ42b5yJfQc" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="favicon" href="favicon.ico">
        <link rel="favicon" href="favicon.png">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <div id="coming-soon">
            <div class="container">
                <div id="logo" class="row">
                    <div class="span8 offset2">
                        <img src="img/logo.png">
                    </div>
                </div>
                <div id="intro" class="row">
                    <div class="span12">
                        <h1>We're Coming Soon</h1>
                        <p>Eminence Advisory was established in 2014 to help businesses and individuals turn creative ideas into successful businesses.<br> The company seeks to be an active agent in Africa’s rapid growth story
                            by driving forward innovative ideas every step of the way.</p><br>
                        <p>Please bare with us whilst we prepare our website for you. <br> Sign up to the mailing list below and we’ll let you know when we're ready!</p>
                    </div>
                </div>
            </div>
            <div id="footer">
                <div class="container">
                    <div class="row">
                        <div class="span12 social">
                            <h2>Follow Us</h2>
                            <ul class="inline">
                                <li><a href="mailto:info@eminenceadvisory.com" class="icon-mail"></a></li>
                                <li><a href="http://www.facebook.com/pages/Eminence-Advisory/641212426006565" target="_blank" class="icon-facebook2"></a></li>
                                <li><a href="http://www.twitter.com/EminenceAfrica" target="_blank" class="icon-twitter2"></a></li>
                                <li><a href="https://plus.google.com/108174460209755630270" target="_blank" rel="publisher" class="icon-googleplus2"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span12 mailing-list">
                            <h2>Join Our Mailing List</h2>
                            <div class="input-append">
                                <form id="mailing-list" method="POST" action="registerAction.php">
                                    <input class="span2" id="" type="text" name="email" placeholder="Email address here...">
                                    <button class="btn" type="submit">Sign Up</button>
                                </form>

                            </div>
                            <div class="join-response"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span12">
                            <p class="copyright">This site is owned and operated by Eminence Advisory Limited.<br> Registered in England & Wales under 09354745 at Kemp House, 152 City Road, London, EC1V 2NX</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/bootstrap-button.js"></script>
        <script src="js/main.js"></script>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-59463655-1', 'auto');
            ga('send', 'pageview');

        </script>
    </body>
</html>
