<?php
/**
 * Created by IntelliJ IDEA.
 * User: okyerk01
 * Date: 15/04/2013
 * Time: 11:39
 * To change this template use File | Settings | File Templates.
 */

$email = strip_tags($_POST['email']);
//store the status and the message in a associative array
$callback = array(
    'status' => null,
    'message' => null
);

if(empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $callback['status'] = 'error';
    $callback['message'] = 'Please enter a valid email';
} else {

//send an email to user to inform them
    $to = "info@eminenceadvisory.com";
    $subject = "New subscriber to mailing list - (".$email.")";
    $body =
        (".$email.") . ": Joined your mailing list.";
    $headers = "From: " . $email . "\r\n";
    $headers .= "Reply-To: ". $email ."\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    $headers .= "X-Mailer: PHP/".phpversion();

    //send email
    if (mail($to, $subject, $body, $headers)) {
        $callback['status'] = 'success';
        $callback['message'] = 'Thank you for registering to our mailing list, we\'ll be in touch soon';
    } else {
        $callback['status'] = 'error';
        $callback['message'] = 'Something went wrong, please try again';
    };

}

echo json_encode($callback);
